use std::io::{prelude::*, BufReader, BufWriter};
use std::net::{TcpListener, TcpStream};
use std::{env, fs, str};

const WEB_ROOT: &'static str = "./webroot";

fn handle_client(stream: TcpStream) -> std::io::Result<()> {
    eprintln!("connected from {}", stream.peer_addr().unwrap());
    let mut tcp_reader = BufReader::new(&stream);
    let mut tcp_writer = BufWriter::new(&stream);
    let tcp_buf = tcp_reader.fill_buf()?;
    let mut header = str::from_utf8(&tcp_buf).unwrap().split_whitespace();
    println!("  received packet:");
    str::from_utf8(tcp_buf)
        .unwrap()
        .split("\n")
        .for_each(|line| println!("    {}", line));
    match (header.next(), header.next(), header.next()) {
        (None, _, _) => eprintln!("      error: packet kind not found"),
        (_, None, _) => eprintln!("      error: required file not found"),
        (_, _, None) => eprintln!("      error: protocol type not found"),
        (Some(kind), Some(file), Some(prot)) => {
            match (kind == "GET", kind == "HEAD", prot.contains("HTTP/")) {
                (_, _, false) => eprintln!("      error: packet is not http"),
                (false, false, _) => eprintln!("      error: not GET or HEAD"),
                (is_get, _, _) => {
                    let file_path = match file {
                        "/" => format!("{}/index.html", WEB_ROOT),
                        _ => format!("{}{}", WEB_ROOT, file),
                    };
                    match fs::OpenOptions::new().read(true).open(&file_path) {
                        Ok(file) => {
                            let mut file_reader = BufReader::new(&file);
                            let mut file_buf = Vec::new();
                            file_reader.read_to_end(&mut file_buf)?;
                            eprintln!("      opened file: {}", file_path);
                            eprintln!("      200 OK");
                            tcp_writer.write_all(
                                b"HTTP/1.0 200 OK\r\n\
                                      Server: Tiny webserver\r\n\r\n",
                            )?;
                            if is_get {
                                tcp_writer.write_all(&file_buf)?;
                            }
                        }
                        Err(e) => {
                            eprintln!("      error: {}", e);
                            tcp_writer.write_all(
                                b"HTTP/1.0 404 NOT FOUND\r\n\
                                      Server: Tiny webserver\r\n\r\n\
                                      <html><head><title>404 Not Found</title></head>\
                                      <body><h1>URL not found</h1></body></html>\r\n",
                            )?;
                        }
                    }
                }
            }
        }
    }
    tcp_writer.flush()?;
    println!("closed\n");
    Ok(())
}

fn main() -> std::io::Result<()> {
    let port = env::args().skip(1).next().unwrap_or(String::from("80"));
    let listner = TcpListener::bind(format!("0.0.0.0:{}", port))?;

    for stream in listner.incoming() {
        handle_client(stream?)?;
    }
    Ok(())
}
